USE [gestoredipro]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[user_balance_mock](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[safepay_user_id] [int] NULL,
	[type] [int] NULL,
	[balance] [decimal](14, 2) NULL
) ON [PRIMARY]
GO

INSERT INTO dbo.user_balance_mock (safepay_user_id, type, balance) VALUES (1, 1, 1.10);
INSERT INTO dbo.user_balance_mock (safepay_user_id, type, balance) VALUES (1, 2, 1.20);
INSERT INTO dbo.user_balance_mock (safepay_user_id, type, balance) VALUES (1, 3, 1.30);
INSERT INTO dbo.user_balance_mock (safepay_user_id, type, balance) VALUES (2, 1, 2.10);
INSERT INTO dbo.user_balance_mock (safepay_user_id, type, balance) VALUES (2, 2, 2.20);
INSERT INTO dbo.user_balance_mock (safepay_user_id, type, balance) VALUES (2, 3, 2.30);
INSERT INTO dbo.user_balance_mock (safepay_user_id, type, balance) VALUES (3, 1, 3.10);
INSERT INTO dbo.user_balance_mock (safepay_user_id, type, balance) VALUES (3, 2, 3.20);
INSERT INTO dbo.user_balance_mock (safepay_user_id, type, balance) VALUES (3, 3, 3.30);