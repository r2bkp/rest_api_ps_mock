package com.mock.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mock.entity.UserBalanceMockEntity;
import com.mock.repository.UserBalanceMockRepository;
import com.mock.response.BalanceItemResponse;
import com.mock.response.BalanceResponse;

@Service
public class MockService {
	
	@Autowired
	private UserBalanceMockRepository userBalanceMockRepository;
	
	public List<BalanceResponse> getUserBalance(Long userId){
		List<BalanceResponse> balanceResponseList = new ArrayList<BalanceResponse>();
		List<BalanceItemResponse> balanceItemResponseList = new ArrayList<BalanceItemResponse>();
		List<UserBalanceMockEntity> userBalanceMockEntityList = userBalanceMockRepository.findAllBySafePayUserIdOrderBySafePayUserId(userId);
		userBalanceMockEntityList.forEach(entity -> balanceItemResponseList.add(new BalanceItemResponse(entity.getType(), entity.getBalance())) );
		balanceResponseList.add(new BalanceResponse(userId, balanceItemResponseList));
		return balanceResponseList;
	}

}
