package com.mock.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "user_balance_mock")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserBalanceMockEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int", name = "id")
    private Long id;
	
    @Column(name = "safepay_user_id", columnDefinition = "int", nullable = false)
    private Long safePayUserId;
    
    @Column(name = "type", columnDefinition = "int", nullable = false)
    private Long type;
    
    @Column(name = "balance", columnDefinition = "decimal(14,2)", nullable = false)
    private BigDecimal balance;

}
