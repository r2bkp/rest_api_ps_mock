package com.mock.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.Gson;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class BalanceResponse {

    private Long safepay_user_id;
    private List<BalanceItemResponse> balances;

    public Long getSafepayUserId() {
        return safepay_user_id;
    }

    public void setSafepayUserId(Long safepayUserId) {
        this.safepay_user_id = safepayUserId;
    }

    public List<BalanceItemResponse> getBalances() {
        return balances;
    }

    public void setBalances(List<BalanceItemResponse> balances) {
        this.balances = balances;
    }
    
    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
