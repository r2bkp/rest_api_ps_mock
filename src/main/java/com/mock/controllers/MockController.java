package com.mock.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mock.external.BalanceItemResponse;
import com.mock.external.BalanceResponse;
import com.mock.external.ClientAccountBalanceEntity;
import com.mock.external.ClienteRepository;
import com.mock.external.UserBalanceService;
import com.mock.service.MockService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/mock")
public class MockController {
	
	@Autowired
	private MockService mockService;
	
	@Autowired
	private UserBalanceService userBalanceService;
	
    @Autowired
    private ClienteRepository clienteRepository;
    

	@GetMapping("/{clientId}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Obtém lista de saldos por cliente mock", response = String.class, httpMethod = "GET")
    public List<BalanceResponse> getUserBalance(@PathVariable("clientId") Long userId) throws IOException {
    	
		
		//TESTE CONSULTA
		LocalDate date = LocalDate.of(2019, Month.APRIL, 30);
		List<ClientAccountBalanceEntity> list = userBalanceService.retrieveUserBalance(69178301L, date);
		

		
		//TESTE SALVAR ENTITY
		date = LocalDate.of(2019, Month.APRIL, 29);
		ClientAccountBalanceEntity clAccBalanEntity = new ClientAccountBalanceEntity();
		clAccBalanEntity.setCliente(clienteRepository.getOne(2725L));
		clAccBalanEntity.setClienteId(2725L);
		clAccBalanEntity.setDate(date);
		clAccBalanEntity.setInitialBalance(new BigDecimal(25));
		clAccBalanEntity.setFinalBalance(new BigDecimal(30));
		clAccBalanEntity.setType(2L);
		List<ClientAccountBalanceEntity> entityList = new ArrayList<ClientAccountBalanceEntity>();
		entityList.add(clAccBalanEntity);
		userBalanceService.saveUserBalance(entityList);
		

		//TESTE SALVAR ARRAY RESPONSE
		BalanceResponse[] balanceResponseArray = new BalanceResponse[1];
		BalanceItemResponse item = new BalanceItemResponse();
		item.setType("1");
		item.setBalance(11.11);
		List<BalanceItemResponse> itemList = new ArrayList<BalanceItemResponse>();
		itemList.add(item);
		BalanceResponse balanceResponse = new BalanceResponse();
		balanceResponse.setSafepayUserId(12365L);
		balanceResponse.setBalances(itemList);
		balanceResponseArray[0] = balanceResponse;
		userBalanceService.saveUserBalance(balanceResponseArray);
				

    	return null; //mockService.getUserBalance(userId);
    }
}