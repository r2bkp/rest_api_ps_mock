package com.mock.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mock.entity.UserBalanceMockEntity;

@Repository
public interface UserBalanceMockRepository extends JpaRepository<UserBalanceMockEntity, Long> {
	
	List<UserBalanceMockEntity> findAllBySafePayUserIdOrderBySafePayUserId(Long safepayUserId);

}
