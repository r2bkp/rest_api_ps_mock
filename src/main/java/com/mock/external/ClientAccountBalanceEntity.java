package com.mock.external;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "client_account_balance")
@IdClass(ClientAccountBalanceId.class)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ClientAccountBalanceEntity {

    @ManyToOne
    @JoinColumn(name = "cliente_id", columnDefinition = "int", insertable = false, updatable = false)
    private ClienteEntity cliente;
    
    @Id
    @Column(name = "cliente_id", columnDefinition = "int", nullable = false)
    private Long clienteId;

    @Id
    @Column(name = "ind_type", columnDefinition = "int", nullable = false)
    private Long type;

    @Id
    @Column(name = "date", columnDefinition = "datetime", nullable = false)
    private LocalDate date;

    @Column(name = "initial_balance", columnDefinition = "decimal(14,2)", nullable = false)
    private BigDecimal initialBalance;
    
    @Column(name = "final_balance", columnDefinition = "decimal(14,2)", nullable = false)
    private BigDecimal finalBalance;

}
