package com.mock.external;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cliente_fluxo")
public class ClienteFluxoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cliente_fluxo_id", columnDefinition = "int")
    private Long id;

    @Column(name = "status", columnDefinition = "bit")
    private Boolean status;

    @ManyToOne
    @JoinColumn(name = "cliente_id", columnDefinition = "int")
    private ClienteEntity cliente;

    @ManyToOne
    @JoinColumn(name = "fluxo_id", columnDefinition = "int")
    private FluxoEntity fluxo;

    @Column(name = "data", columnDefinition = "datetime")
    private LocalDateTime data;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "fluxo", targetEntity = ClienteFluxoItemsEntity.class)
    private List<ClienteFluxoItemsEntity> fluxoItems = new LinkedList<>();

    public LocalDateTime getData() {
	return data;
    }

    public void setData(LocalDateTime data) {
	this.data = data;
    }

    public List<ClienteFluxoItemsEntity> getFluxoItems() {
	return fluxoItems;
    }

    public void setFluxoItems(List<ClienteFluxoItemsEntity> fluxoItems) {
	this.fluxoItems = fluxoItems;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Boolean getStatus() {
	return status;
    }

    public void setStatus(Boolean status) {
	this.status = status;
    }

    public ClienteEntity getCliente() {
	return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
	this.cliente = cliente;
    }

    public FluxoEntity getFluxo() {
	return fluxo;
    }

    public void setFluxo(FluxoEntity fluxo) {
	this.fluxo = fluxo;
    }

}
