package com.mock.external;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cliente_versao_edi")
public class ClienteVersaoEdiEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cliente_versao_edi_id", columnDefinition = "int")
    private Long clienteVersaoEdiId;

    @ManyToOne
    @JoinColumn(name = "cliente_id", columnDefinition = "int")
    private ClienteEntity cliente;

    @ManyToOne
    @JoinColumn(name = "versao_edi_id", columnDefinition = "int")
    private VersaoEdiEntity versaoEdi;

    @Column(name = "data_criacao", columnDefinition = "datetime")
    private LocalDateTime dataCriacao;

    public Long getClienteVersaoEdiId() {
	return clienteVersaoEdiId;
    }

    public void setClienteVersaoEdiId(Long clienteVersaoEdiId) {
	this.clienteVersaoEdiId = clienteVersaoEdiId;
    }

    public ClienteEntity getCliente() {
	return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
	this.cliente = cliente;
    }

    public VersaoEdiEntity getVersaoEdi() {
	return versaoEdi;
    }

    public void setVersaoEdi(VersaoEdiEntity versaoEdi) {
	this.versaoEdi = versaoEdi;
    }

    public LocalDateTime getDataCriacao() {
	return dataCriacao;
    }

    public void setDataCriacao(LocalDateTime dataCriacao) {
	this.dataCriacao = dataCriacao;
    }

}
