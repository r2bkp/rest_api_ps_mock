package com.mock.external;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cliente_documentos")
public class ClienteDocumentoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cliente_documentos_id", columnDefinition = "int")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "documento_id", columnDefinition = "int")
    private DocumentoEntity documento;

    @ManyToOne
    @JoinColumn(name = "cliente_id", columnDefinition = "int")
    private ClienteEntity cliente;

    @Column(name = "conteudo", columnDefinition = "varchar(max)")
    private String conteudo;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public DocumentoEntity getDocumento() {
	return documento;
    }

    public void setDocumento(DocumentoEntity documento) {
	this.documento = documento;
    }

    public ClienteEntity getCliente() {
	return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
	this.cliente = cliente;
    }

    public String getConteudo() {
	return conteudo;
    }

    public void setConteudo(String conteudo) {
	this.conteudo = conteudo;
    }

}
