package com.mock.external;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientAccountBalanceRepository extends JpaRepository<ClientAccountBalanceEntity, ClientAccountBalanceId> {
	
	List<ClientAccountBalanceEntity> findAllByClienteAndDateOrderByCliente(ClienteEntity cliente, LocalDate date);

}
