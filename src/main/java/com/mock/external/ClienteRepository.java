package com.mock.external;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
// @Transactional(propagation = Propagation.)
public interface ClienteRepository extends JpaRepository<ClienteEntity, Long> {

	List<ClienteEntity> findAllByMatrizId(Long id);

	List<ClienteEntity> findAllByEdiAtivoIsTrue();

	List<ClienteEntity> findAllByMatrizIdAndEdiAtivoIsFalse(Long id);

	Boolean existsByIdAndViradaR2IsTrue(Long id);

	List<ClienteEntity> findAllByEdiAtivoIsTrueAndDataAtivacaoGreaterThan(LocalDateTime dataCriacao);

	List<ClienteEntity> findAllByEdiAtivoIsTrueAndGeraEmHorarioDiferentao(Integer horarioDiferentao);

	List<ClienteEntity> findAllByEdiAtivoIsTrueAndViradaR2IsTrue();

	Page<ClienteEntity> findAllByEdiAtivoIsTrue(Pageable pageable);

	Page<ClienteEntity> findAllByEdiAtivoIsTrueAndGeraEmHorarioDiferentao(Integer geraEmHorarioDiferentao, //
			Pageable pageable);

	Optional<ClienteEntity> findByIdAndEdiAtivoIsTrue(Long clientId);
	
	List<ClienteEntity> findAllByTopaz(Boolean topaz);
	
	Optional<ClienteEntity> findById(Long clientId);
}
