package com.mock.external;

import java.util.stream.Stream;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "versao_edi")
public class VersaoEdiEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "versao_edi_id", columnDefinition = "int")
	private Long versao_edi_id;

	@Column(name = "numero", columnDefinition = "varchar(7)")
	private String numero;

	@Column(name = "descricao", columnDefinition = "varchar(max)")
	private String descricao;

	@Column(name = "layout", columnDefinition = "varbinary(max)")
	private byte[] layout;

	@Column(name = "padrao", columnDefinition = "bit")
	private boolean padrao;

	public boolean getPadrao() {
		return padrao;
	}

	public void setPadrao(boolean padrao) {
		this.padrao = padrao;
	}

	public Long getVersao_edi_id() {
		return versao_edi_id;
	}

	public void setVersao_edi_id(Long versao_edi_id) {
		this.versao_edi_id = versao_edi_id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public byte[] getLayout() {
		return layout;
	}

	public void setLayout(byte[] layout) {
		this.layout = layout;
	}

	public static enum EDIVersion {

		V1_05("00105"),

		V2_00_2B("0022b"),

		V2_00_2C("0022c"),

		V2_01_00("00201")

		;

		private EDIVersion(final String code) {
			this.code = code;
		}

		private final String code;

		public String getCode() {
			return code;
		}

		public static EDIVersion fromBy(final String code) {
			return Stream.of(EDIVersion.values())//
					.filter(it -> it.getCode().equals(code))//
					.findFirst()//
					.orElseThrow(IllegalStateException::new);
		}

	}

}
