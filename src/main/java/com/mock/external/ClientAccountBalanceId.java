package com.mock.external;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ClientAccountBalanceId implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long clienteId;
    private Long type;
    private LocalDate date;

}
