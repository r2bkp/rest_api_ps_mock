package com.mock.external;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cliente_fluxo_items")
public class ClienteFluxoItemsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cliente_fluxo_items_id", columnDefinition = "int")
    private Long id;

    @Column(name = "status", columnDefinition = "bit")
    private Boolean status;

    @ManyToOne
    @JoinColumn(name = "fluxo_items_id", columnDefinition = "int")
    private FluxoItemsEntity fluxoItem;

    @ManyToOne
    @JoinColumn(name = "fluxo_id", columnDefinition = "int")
    private ClienteFluxoEntity fluxo;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Boolean getStatus() {
	return status;
    }

    public void setStatus(Boolean status) {
	this.status = status;
    }

    public FluxoItemsEntity getFluxoItems() {
	return fluxoItem;
    }

    public void setFluxoItems(FluxoItemsEntity fluxoItems) {
	this.fluxoItem = fluxoItems;
    }

    public ClienteFluxoEntity getFluxo() {
	return fluxo;
    }

    public void setFluxo(ClienteFluxoEntity fluxo) {
	this.fluxo = fluxo;
    }

}
