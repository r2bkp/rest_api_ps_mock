package com.mock.external;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name = "fluxo")
public class FluxoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fluxo_id", columnDefinition = "int")
    private Long id;

    @Column(name = "descricao", columnDefinition = "varchar(max)")
    private String descricao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "fluxo", targetEntity = FluxoItemsEntity.class)
    private List<FluxoItemsEntity> items = new LinkedList<>();

    public List<FluxoItemsEntity> getItems() {
	return items;
    }

    public void setItems(List<FluxoItemsEntity> items) {
	this.items = items;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getDescricao() {
	return descricao;
    }

    public void setDescricao(String descricao) {
	this.descricao = descricao;
    }

    @Override
    public String toString() {
	return "";//ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
