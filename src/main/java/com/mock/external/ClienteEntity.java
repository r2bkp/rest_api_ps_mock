package com.mock.external;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cliente")
public class ClienteEntity {
	@Id
	@Column(columnDefinition = "int", name = "cliente_id", nullable = false)
	private Long id;

	@Column(columnDefinition = "varchar(max)", name = "nome")
	private String nome;

	@Column(columnDefinition = "varchar(max)", name = "nome_fantasia")
	private String nomeFantasia;

	@Column(columnDefinition = "varchar(max)", name = "email")
	private String email;

	@ManyToOne
	@JoinColumn(columnDefinition = "int", name = "matriz_id")
	private ClienteEntity matriz;

	@Column(columnDefinition = "varchar(max)", name = "contato_nome")
	private String contatoNome;

	@Column(columnDefinition = "varchar(100)", name = "contato_email")
	private String contatoEmail;

	@Column(columnDefinition = "varchar(20)", name = "contato_telefone")
	private String contatoTelefone;

	@Column(columnDefinition = "bit", name = "edi_ativo", nullable = false)
	private Boolean ediAtivo;

	@Column(name = "id_fise", columnDefinition = "varchar(32)")
	private String idFise;

	@Column(name = "data_ativacao", columnDefinition = "datetime")
	private LocalDateTime dataAtivacao;

	@Column(name = "gerou_fise", columnDefinition = "bit")
	private Boolean gerouFise;

	@Column(name = "ativacao_apollo", columnDefinition = "bit")
	private Boolean ativacaoApollo;

	@Column(name = "status", columnDefinition = "bit")
	private Boolean status;

	@Column(name = "prioridade", columnDefinition = "char(1)")
	private String prioridade;

	@Column(name = "virada_r2", columnDefinition = "bit")
	private Boolean viradaR2;

	@Column(name = "observacao", columnDefinition = "varchar(max)")
	private String observacao;

	@Column(name = "aging", columnDefinition = "int")
	private Integer aging;

	@Column(name = "id_integracao", columnDefinition = "varchar(32)")
	private String idIntegracao;

	@Column(name = "nome_comercial", columnDefinition = "varchar(max)")
	private String nomeComercial;

	@Column(name = "email_comercial", columnDefinition = "varchar(max)")
	private String emailComercial;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente", targetEntity = ClienteVersaoEdiEntity.class)
	private List<ClienteVersaoEdiEntity> versoesEdi = new LinkedList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente", targetEntity = ClienteTipoArquivosEntity.class)
	private List<ClienteTipoArquivosEntity> tiposArquivo = new LinkedList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente", targetEntity = ClienteVanEntity.class)
	private List<ClienteVanEntity> vans = new LinkedList<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente", targetEntity = ClienteFluxoEntity.class)
	private List<ClienteFluxoEntity> fluxos = new LinkedList<>();
	
	@Column(columnDefinition = "bit", name = "topaz", nullable = true)
	private Boolean topaz;

	public List<ClienteVanEntity> getVans() {
		return vans;
	}

	public void setVans(List<ClienteVanEntity> vans) {
		this.vans = vans;
	}

	public List<ClienteFluxoEntity> getFluxos() {
		return fluxos;
	}

	public void setFluxos(List<ClienteFluxoEntity> fluxos) {
		this.fluxos = fluxos;
	}

	public List<ClienteTipoArquivosEntity> getTiposArquivo() {
		return tiposArquivo;
	}

	public void setTiposArquivo(List<ClienteTipoArquivosEntity> tiposArquivo) {
		this.tiposArquivo = tiposArquivo;
	}

	@Column(name = "geracao_alternativa", columnDefinition = "int")
	private Integer geraEmHorarioDiferentao;

	public Integer getGeraEmHorarioDiferentao() {
		return geraEmHorarioDiferentao;
	}

	public void setGeraEmHorarioDiferentao(Integer geraEmHorarioDiferentao) {
		this.geraEmHorarioDiferentao = geraEmHorarioDiferentao;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente", targetEntity = ClienteDocumentoEntity.class)
	private List<ClienteDocumentoEntity> clienteDocumentos = new LinkedList<>();

	public List<ClienteDocumentoEntity> getClienteDocumentos() {
		return clienteDocumentos;
	}

	public void setClienteDocumentos(List<ClienteDocumentoEntity> clienteDocumentos) {
		this.clienteDocumentos = clienteDocumentos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ClienteEntity getMatriz() {
		return matriz;
	}

	public void setMatriz(ClienteEntity matriz) {
		this.matriz = matriz;
	}

	public String getContatoNome() {
		return contatoNome;
	}

	public void setContatoNome(String contatoNome) {
		this.contatoNome = contatoNome;
	}

	public String getContatoEmail() {
		return contatoEmail;
	}

	public void setContatoEmail(String contatoEmail) {
		this.contatoEmail = contatoEmail;
	}

	public String getContatoTelefone() {
		return contatoTelefone;
	}

	public void setContatoTelefone(String contatoTelefone) {
		this.contatoTelefone = contatoTelefone;
	}

	public Boolean getEdiAtivo() {
		return ediAtivo;
	}

	public void setEdiAtivo(Boolean ediAtivo) {
		this.ediAtivo = ediAtivo;
	}

	public String getIdFise() {
		return idFise;
	}

	public void setIdFise(String idFise) {
		this.idFise = idFise;
	}

	public LocalDateTime getDataAtivacao() {
		return dataAtivacao;
	}

	public void setDataAtivacao(LocalDateTime dataAtivacao) {
		this.dataAtivacao = dataAtivacao;
	}

	public Boolean getGerouFise() {
		return gerouFise;
	}

	public void setGerouFise(Boolean gerouFise) {
		this.gerouFise = gerouFise;
	}

	public Boolean getAtivacaoApollo() {
		return ativacaoApollo;
	}

	public void setAtivacaoApollo(Boolean ativacaoApollo) {
		this.ativacaoApollo = ativacaoApollo;
	}

	public Boolean getStatus() {
		return Objects.nonNull(status) ? status : false;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}

	public List<ClienteVersaoEdiEntity> getVersoesEdi() {
		return versoesEdi;
	}

	public void setVersoesEdi(List<ClienteVersaoEdiEntity> versoesEdi) {
		this.versoesEdi = versoesEdi;
	}

	public Boolean getViradaR2() {
		return viradaR2;
	}

	public void setViradaR2(Boolean viradaR2) {
		this.viradaR2 = viradaR2;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Integer getAging() {
		return aging;
	}

	public void setAging(Integer aging) {
		this.aging = aging;
	}

	public String getIdIntegracao() {
		return idIntegracao;
	}

	public void setIdIntegracao(String idIntegracao) {
		this.idIntegracao = idIntegracao;
	}

	public String getNomeComercial() {
		return nomeComercial;
	}

	public void setNomeComercial(String nomeComercial) {
		this.nomeComercial = nomeComercial;
	}

	public String getEmailComercial() {
		return emailComercial;
	}

	public void setEmailComercial(String emailComercial) {
		this.emailComercial = emailComercial;
	}
	

//    @Override
//    public String toString() {
//	if (Objects.nonNull(contatoNome)) {
//	    return this.id + "|" + this.nome + "|" + this.email + "|" + this.ediAtivo + "|" + this.contatoNome;
//	}
//	return this.id + "|" + this.nome + "|" + this.email + "|" + this.ediAtivo;
//	// return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
//    }

	public Boolean getTopaz() {
		return topaz;
	}

	public void setTopaz(Boolean topaz) {
		this.topaz = topaz;
	}

	public ClienteEntity(Long id) {
		super();
		this.id = id;
	}

	public ClienteEntity() {
		super();

	}

}
