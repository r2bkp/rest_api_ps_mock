package com.mock.external;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "fluxo_items")
public class FluxoItemsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fluxo_items_id", columnDefinition = "int")
    private Long id;

    @Column(name = "descricao", columnDefinition = "varchar(max)")
    private String descricao;

    @ManyToOne
    @JoinColumn(name = "fluxo_id", columnDefinition = "int")
    private FluxoEntity fluxo;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getDescricao() {
	return descricao;
    }

    public void setDescricao(String descricao) {
	this.descricao = descricao;
    }

    public FluxoEntity getFluxo() {
	return fluxo;
    }

    public void setFluxo(FluxoEntity fluxo) {
	this.fluxo = fluxo;
    }

    @Override
    public String toString() {
	return this.descricao;
    }

}
