package com.mock.external;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipo_arquivo")
public class TipoArquivoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "int", name = "tipo_arquivo_id")
	private Long id;

	@Column(columnDefinition = "varchar(36)", name = "codigo")
	private String codigo;

	@Column(columnDefinition = "varchar(max)", name = "descricao")
	private String descricao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

//	public static enum FileType {
//
//		TRANSACTIONAL_COMPLETE("1", "TRANS", TipoMovimento.TRANSACIONAL, TipoExtrato.TRANSACTIONAL_COMPLETE),
//
//		TRANSACTIONAL_SUMMARY("2", "TRANSSUMMARY", TipoMovimento.TRANSACIONAL, TipoExtrato.TRANSACTIONAL_SUMMARY),
//
//		FINANCIAL("3", "FIN", TipoMovimento.FINANCEIRO, TipoExtrato.FINANCIAL),
//
//		CASH_ADVANCE("4", "ANT", TipoMovimento.FINANCEIRO, TipoExtrato.CASH_ADVANCE)
//
//		;
//
//		private FileType(final String code, //
//				final String fileName, //
//				final TipoMovimento movementType, //
//				final TipoExtrato extractType) {
//			this.code = code;
//			this.fileName = fileName;
//			this.movementType = movementType;
//			this.extractType = extractType;
//		}
//
//		private final String code;
//
//		private final String fileName;
//
//		private final TipoMovimento movementType;
//
//		private final TipoExtrato extractType;
//
//		public String getCode() {
//			return code;
//		}
//
//		public String getFileName() {
//			return fileName;
//		}
//
//		public TipoMovimento getMovementType() {
//			return movementType;
//		}
//
//		public TipoExtrato getExtractType() {
//			return extractType;
//		}
//
//		public static FileType fromBy(final String code) {
//			return Stream.of(FileType.values())//
//					.filter(it -> it.getCode().equals(code))//
//					.findFirst()//
//					.orElseThrow(IllegalStateException::new);
//		}
//
//	}

}
