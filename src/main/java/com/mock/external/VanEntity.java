package com.mock.external;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "van")
public class VanEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int", name = "van_id", nullable = false)
    private Long id;

    @Column(columnDefinition = "varchar(36)", name = "codigo")
    private String codigo;

    @Column(columnDefinition = "varchar(max)", name = "nome")
    private String nome;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getCodigo() {
	return codigo;
    }

    public void setCodigo(String codigo) {
	this.codigo = codigo;
    }

    public String getNome() {
	return nome;
    }

    public void setNome(String nome) {
	this.nome = nome;
    }

    @Override
    public String toString() {
	return "";//ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
