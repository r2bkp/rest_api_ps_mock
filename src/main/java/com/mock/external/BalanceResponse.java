package com.mock.external;

import java.util.List;

public class BalanceResponse {

    private Long safepay_user_id;
    private List<BalanceItemResponse> balances;

    public Long getSafepayUserId() {
        return safepay_user_id;
    }

    public void setSafepayUserId(Long safepayUserId) {
        this.safepay_user_id = safepayUserId;
    }

    public List<BalanceItemResponse> getBalances() {
        return balances;
    }

    public void setBalances(List<BalanceItemResponse> balances) {
        this.balances = balances;
    }

}
