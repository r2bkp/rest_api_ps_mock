package com.mock.external;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserBalanceService {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(UserBalanceService.class);
    
    @Autowired
    private ClientAccountBalanceRepository userBalanceRepository;
    
    @Autowired
    private ClienteRepository clienteRepository;
    

    public void saveUserBalance(BalanceResponse[] balanceResponseArray) {
    	
    	List<ClientAccountBalanceEntity> clientAccountBalanceEntityList = new ArrayList<ClientAccountBalanceEntity>();
 
    	Arrays.asList(balanceResponseArray).forEach(balance -> {
    		balance.getBalances().forEach(balanceItem -> {
    			ClientAccountBalanceEntity entity =  new ClientAccountBalanceEntity();
    			//entity.setSafepayUserId(balance.getSafepayUserId());
    			entity.setCliente(clienteRepository.getOne(balance.getSafepayUserId()));
    			entity.setClienteId(balance.getSafepayUserId());
    			entity.setType(Long.valueOf(balanceItem.getType()));
    			entity.setDate(LocalDate.now());
    			entity.setInitialBalance(BigDecimal.valueOf(balanceItem.getBalance()));
    			entity.setFinalBalance(BigDecimal.valueOf(balanceItem.getBalance()));
    			clientAccountBalanceEntityList.add(entity);
    		});
    	});
   	
    	userBalanceRepository.saveAll(clientAccountBalanceEntityList);
    }
    
    public void saveUserBalance(List<ClientAccountBalanceEntity> clientAccountBalanceEntityList) {
    	userBalanceRepository.saveAll(clientAccountBalanceEntityList);
    }
   
    public List<ClientAccountBalanceEntity> retrieveUserBalance(Long clientId, LocalDate date){
    	return userBalanceRepository.findAllByClienteAndDateOrderByCliente(clienteRepository.getOne(clientId), date);
    }
}
