package com.mock.external;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cliente_van")
public class ClienteVanEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int", name = "cliente_van_id", nullable = false)
    private Long id;

    @Column(columnDefinition = "bit", name = "status", nullable = false)
    private Boolean status;

    @ManyToOne
    @JoinColumn(columnDefinition = "int", name = "cliente_id", nullable = false)
    private ClienteEntity cliente;

    @ManyToOne
    @JoinColumn(columnDefinition = "int", name = "van_id", nullable = false)
    private VanEntity van;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Boolean getStatus() {
	return status;
    }

    public void setStatus(Boolean status) {
	this.status = status;
    }

    public ClienteEntity getCliente() {
	return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
	this.cliente = cliente;
    }

    public VanEntity getVan() {
	return van;
    }

    public void setVan(VanEntity van) {
	this.van = van;
    }

}
