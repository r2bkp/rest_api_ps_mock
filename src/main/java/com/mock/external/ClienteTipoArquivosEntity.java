package com.mock.external;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cliente_tipo_arquivos")
public class ClienteTipoArquivosEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cliente_tipo_arquivos_id", columnDefinition = "int")
	private Long id;

	@Column(name = "data", columnDefinition = "datetime")
	private LocalDateTime data;

	@Column(name = "status", columnDefinition = "bit")
	private Boolean status;

	@ManyToOne
	@JoinColumn(name = "cliente_id", columnDefinition = "int")
	private ClienteEntity cliente;

	@ManyToOne
	@JoinColumn(name = "tipo_arquivo_id", columnDefinition = "int")
	private TipoArquivoEntity tipoArquivo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public ClienteEntity getCliente() {
		return cliente;
	}

	public void setCliente(ClienteEntity cliente) {
		this.cliente = cliente;
	}

	public TipoArquivoEntity getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(TipoArquivoEntity tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}

}
